# PP Order in English vs Chinese


ptb.py extracts PTB-style data for the PPs, the original sentences and the structural variants where the order of the PPs is switched;

dlm-permutation.py conducts Monte Carlo permutation test for effects of dependency length;

sc-permutation.py conducts Monte Carlo permutation test for the effects of argument status;

length-compare evaluates the relationship between dependency length and argument status;

data-process.py generates data in csv format that is subject to logistic regresison modeling;

pp-phrase-structure.R contains codes for generating graphs in the paper as well as for logistic regression modeling;
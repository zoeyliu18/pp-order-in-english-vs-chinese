#usr/bin/env python3
import sys, re, glob, os, csv, io
import numpy as np
from collections import Counter
from numpy import random
import statistics

path = '/Users/Silverlining/Desktop/Experiments/Penn_PP/'
os.chdir(path)

pp1 = []
with open(sys.argv[1] + '-pp1.txt') as f:
	for line in f:
		toks = line.split()
		pp1.append(len(toks))

pp2 = []
with open(sys.argv[1] + '-pp2.txt') as f:
	for line in f:
		toks = line.split()
		pp2.append(len(toks))

data = []
for i in range(len(pp1)):
	data.append(pp1[i])
	data.append(pp2[i])


A = np.array(data)

def permutation_test(pooled, size, delta):
	np.random.shuffle(pooled)
	pooled_sample = pooled[ : size]
	pooled_ave = statistics.mean(pooled_sample.tolist())

	return pooled_ave

delta = 0
size = 50
numSamples = 1000000
ave_list = []

#estimates = []
#results = list(map(lambda x: permutation_test(A, size, delta), range(numSamples)))
#ave_mean = statistics.mean(results)
#ave_std = statistics.pstdev(results)
#print(round(ave_mean,2))
#print(round(ave_std, 2))

len_diff = []
for i in range(len(pp1)):
	len_diff.append(abs(pp2[i] - pp1[i]))

B = np.array(len_diff)

def len_diff_permutation(pooled, size, delta):
	np.random.shuffle(pooled)
	pooled_sample = pooled[ : size]
	small = 0
	other = 0
	for tok in pooled_sample:
		if tok <= 2 and tok > 0:
			small += 1
		else:
			other += 1
	return [small * 100 / size, other * 100 / size]

len_diff_estimates = []
len_diff_results = list(map(lambda x: len_diff_permutation(B, size, delta), range(numSamples)))
small_list = []
other_list = []
for tok in len_diff_results:
	small_list.append(tok[0])
	other_list.append(tok[1])

small_mean = statistics.mean(small_list)
small_std = statistics.pstdev(small_list)
other_mean = statistics.mean(other_list)
other_std = statistics.pstdev(other_list)
print(round(small_mean, 2))
print(round(small_std, 2))

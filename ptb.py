'''This script extracts verb phrases with two PP obliques from Penn treebank'''

import nltk
from nltk.corpus import BracketParseCorpusReader
import numpy as np
import scipy
from scipy import spatial
import matplotlib.pyplot as plt
import math
import re
import sys
import csv

corpus_root = r"all/"
file_pattern= r".*\.mrg"

sw = BracketParseCorpusReader(corpus_root, file_pattern)


trees = sw.parsed_sents()




def give(t):
	return t.label() == 'VP'

all_vp = []

for tree in trees:
	for vp in tree.subtrees(give):
		children = []
		pps = []
		pp = []
		for child in vp:
			if 'PP' in child.label():
				np = []
				labels = []
				for tok in child:
					labels.append(tok.label())
					if 'NP' in tok.label():
						np.append(tok.label())
				if len(np) >= 1:
					if 'VB' not in labels[0]:
						pps.append(child.label())
			
		if len(pps) == 2:
			a = []
			sent=[]
			verb_phrase = []
			for word in tree.leaves():
				if '*' not in word:
					if ' 0' not in word:
						if word != '0':
							sent.append(word)
			for child in vp:
				
				if 'PP' in child.label():
					np = []
					labels = []
					for tok in child:
						labels.append(tok.label())
						if 'NP' in tok.label():
							np.append(tok.label())
					if len(np) >= 1:
						if 'VB' not in labels[0]:
							pp = []
							for word in child.leaves():
								if '*' not in word:
									if ' 0' not in word:
										if word != '0':
											pp.append(word)
							new_pp = ' '.join(word for word in pp)
							a.append([new_pp, len(pp)])
							verb_phrase.append([new_pp, len(pp)])
				else:
					
					for word in child.leaves():
						if '*' not in word:
							if ' 0' not in word:
								if word != '0':
									verb_phrase.append([word])

			a.append(pps)
			a.append(verb_phrase)
			a.append(sent)
			all_vp.append(a)


short_closer = 0
long_closer = 0
equal = 0
MP = 0
MT = 0
PT = 0
PM = 0
TM = 0
TP = 0


A=[]
B=[]
for tok in all_vp:
	vp = []
	for item in tok[3]:
		vp.append(item[0])
	new_vp = ' '.join(word for word in vp)
	reverse_vp = []
	pps = []
	for item in tok[3]:
		if len(item) !=1:
			pps.append(item)
	pp1_id, pp2_id = tok[3].index(pps[0]), tok[3].index(pps[1])
	tok[3][pp1_id], tok[3][pp2_id] = tok[3][pp2_id], tok[3][pp1_id]
	for item in tok[3]:
		reverse_vp.append(item[0])

	new_reverse_vp = ' '.join(word for word in reverse_vp)

	sent = ' '.join(word for word in tok[-1])
	new_sent = re.sub(new_vp, new_reverse_vp, sent)
	tok.append(new_sent.split())

for tok in all_vp:
	a = []
	for item in tok[-1][:]:
		if item.startswith('\\'):
			tok[-1].remove(item)
	a.append(tok[-1])
	pp1 = tok[0][0].split()
	for item in pp1[:]:
		if item.startswith('\\'):
			pp1.remove(item)
	a.append(pp1)
	a.append(tok[2])
	for item in tok[-2][:]:
		if item.startswith('\\'):
			tok[-2].remove(item)
	a.append(tok[-2])
	A.append(a)



for tok in all_vp:
	b = []
	for item in tok[-1][:]:
		if item.startswith('\\'):
			tok[-1].remove(item)
	b.append(tok[-1])
	pp2=tok[1][0].split()
	for item in pp2[:]:
		if item.startswith('\\'):
			pp2.remove(item)
	b.append(pp2)
	b.append(tok[2])
	for item in tok[-2][:]:
		if item.startswith('\\'):
			tok[-2].remove(item)
	b.append(tok[-2])
	B.append(b)

header = ['Sentence', 'Reverse_sentence', 'PP1', 'PP2', 'PP1-len', 'PP2-len', 'PP1-tag', 'PP2-tag']

arguments = ['PP', 'PP-CLR', 'PP-EXT', 'PP-PUT', 'PP-DTV', 'PP-BNF', 'PP-PRD']
adjuncts = ['PP-DIR','PP-LOC', 'PP-MNR', 'PP-PRP', 'PP-TMP']

arg_close = 0
adjunct_close = 0
arg_short_close = 0
arg_long_close = 0
arg_equal = 0
adjunct_short_close = 0
adjunct_long_close = 0
adjunct_equal = 0

original = []
variant = []
all_pp1 = []
all_pp2 = []

arg_pp1 = []
arg_pp2 = []
adjunct_pp1 = []
adjunct_pp2 = []

with open(sys.argv[1] + '-vp-data.csv', 'w')  as data:
	writer = csv.writer(data)
	writer.writerow(header)

	for tok in zip(A, B):
		if len(tok[0][1]) > 1:
			if len(tok[1][1]) > 1:

				tok[0][0].append('<eos>')
				reverse_sent = ' '.join(w for w in tok[0][0])
				reverse_sent = re.sub(r'-LCB-', '(', reverse_sent)
				reverse_sent = re.sub(r'-LRB', '(', reverse_sent)
				reverse_sent = re.sub(r'-RRB-', ')', reverse_sent)
				reverse_sent = re.sub(r'-RCB-', ')', reverse_sent)

				tok[0][-1].append('<eos>')
				sent = ' '.join(w for w in tok[0][-1])
				sent = re.sub(r'-LCB-', '(', sent)
				sent = re.sub(r'-LRB', '(', sent)
				sent = re.sub(r'-RRB-', ')', sent)
				sent = re.sub(r'-RCB-', ')', sent)


				pp1 = ' '.join(w for w in tok[0][1])
				pp1 = re.sub(r'-LCB-', '(', pp1)
				pp1 = re.sub(r'-LRB', '(', pp1)
				pp1 = re.sub(r'-RRB-', ')', pp1)
				pp1 = re.sub(r'-RCB-', ')', pp1)

				pp2 = ' '.join(w for w in tok[1][1])
				pp2 = re.sub(r'-LCB-', '(', pp2)
				pp2 = re.sub(r'-LRB', '(', pp2)
				pp2 = re.sub(r'-RRB-', ')', pp2)
				pp2 = re.sub(r'-RCB-', ')', pp2)

				pp1_tag = tok[0][2][0]
				pp2_tag = tok[0][2][1]

				pp1_semantics = ' '
				pp2_semantics = ' '
				if tok[0][2][1] in arguments and tok[0][2][0] in adjuncts:
					pp1_semantics = 'Adjunct'
					pp2_semantics = 'Argument'

				if tok[0][2][1] in adjuncts and tok[0][2][0] in arguments:	
					pp1_semantics = 'Argument'
					pp2_semantics = 'Adjunct'

				writer.writerow([sent, reverse_sent, pp1, pp2, len(pp1.split()), len(pp2.split()), pp1_tag, pp2_tag, pp1_semantics, pp2_semantics])

				original.append(sent)
				variant.append(reverse_sent)
				all_pp1.append(pp1)
				all_pp2.append(pp2)

				if len(tok[0][1]) < len(tok[1][1]):
					short_closer += 1
				if len(tok[0][1]) > len(tok[1][1]):
					long_closer += 1
				if len(tok[0][1]) == len(tok[1][1]):
					equal += 1
				if tok[0][2] == ['PP-MNR', 'PP-LOC']:
					MP += 1
				if tok[0][2] == ['PP-MNR', 'PP-TMP']:
					MT += 1
				if tok[0][2] == ['PP-LOC', 'PP-TMP']:
					PT += 1
				if tok[0][2] == ['PP-LOC', 'PP-MNR']:
					PM += 1
				if tok[0][2] == ['PP-TMP', 'PP-MNR']:
					TM += 1
				if tok[0][2] == "['PP-TMP', 'PP-LOC']":
					TP += 1
				if tok[0][2][0] in arguments and tok[0][2][1] in adjuncts:
					arg_close += 1
					if len(tok[0][1]) < len(tok[1][1]):
						arg_short_close += 1
					if len(tok[0][1]) > len(tok[1][1]):
						arg_long_close += 1
					if len(tok[0][1]) == len(tok[1][1]):
						arg_equal += 1	
				if tok[0][2][0] in adjuncts and tok[0][2][1] in arguments:	
					adjunct_close += 1
					if len(tok[0][1]) < len(tok[1][1]):
						adjunct_short_close += 1
					if len(tok[0][1]) > len(tok[1][1]):
						adjunct_long_close += 1
					if len(tok[0][1]) == len(tok[1][1]):
						adjunct_equal += 1	

with open('brown-original.txt', 'w') as f:
	for tok in original:
		f.write(tok + '\n')
f.close()

with open('brown-variant.txt', 'w') as f:
	for tok in variant:
		f.write(tok + '\n')
f.close()

with open('brown-pp1.txt', 'w') as f:
	for tok in all_pp1:
		f.write(tok + '\n')
f.close()

with open('brown-pp2.txt', 'w') as f:
	for tok in all_pp2:
		f.write(tok + '\n')
f.close()


print(arg_close)
print(arg_short_close)
print(arg_long_close)
print(arg_equal)	
print(adjunct_close)	
print(adjunct_short_close)
print(adjunct_long_close)
print(adjunct_equal)	
print(short_closer)
print(long_closer)
print(equal)
print(MP)
print(MT)
print(PT)
print(PM)
print(TM)
print(TP)		

'''
627
379
124
124
421
265
63
93
1886
543
604
0
0
78
0
0
0
'''
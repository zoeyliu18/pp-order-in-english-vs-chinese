#usr/bin/env python3
import sys, re, glob, os, csv, io
import numpy as np
from collections import Counter
from numpy import random
import statistics

path = '/Users/Silverlining/Desktop/Experiments/Penn_PP/'
os.chdir(path)

pp1 = []
with open(sys.argv[1] + '-pp1.txt') as f:
	for line in f:
		toks = line.split()
		pp1.append(len(toks))

pp2 = []
with open(sys.argv[1] + '-pp2.txt') as f:
	for line in f:
		toks = line.split()
		pp2.append(len(toks))

short_closer = 0
long_closer = 0
total = len(pp1)

data = []
for i in range(0, len(pp1)):
	data.append(pp2[i] - pp1[i])


A_diff = np.array(data)

def en_permutation_test(pooled, size, delta):
	np.random.shuffle(pooled)
	s = 0
	l = 0
	e = 0
	A_sample = pooled[ : size]
	for i in range(0, size):
		if A_sample[i] >  0:
			s += 1
		if A_sample[i] <  0:
			l += 1
		if A_sample[i] ==  0:
			e += 1
	return [s / size - l / size, s / size, l / size, e / size]

def ja_permutation_test(pooled, size, delta):
	np.random.shuffle(pooled)
#	np.random.shuffle(p1)
#	np.random.shuffle(p2)
	s = 0
	l = 0
	e = 0
	A_sample = pooled[ : size]
#	B_sample = pooled[-size : ]
	for i in range(0, size):
		if A_sample[i] < 0:
			s += 1
		if A_sample[i] >  0:
			l += 1
		if A_sample[i] ==  0:
			e += 1
	return [s / size - l / size, s / size, l / size, e / size]

pooled = np.hstack([A_diff])
#delta = short_closer / total - long_closer / total
delta = 0
#size = int(len(pp1) / 2)
size = 40
numSamples = 1000000
s_list = []
l_list = []
e_list = []
estimates = []
if sys.argv[1] in ['wsj', 'brown', 'swbd']:
	results = list(map(lambda x: en_permutation_test(A_diff, size, delta), range(numSamples)))
	for tok in results:
		estimates.append(tok[0])
#	estimates = np.array(list(map(lambda x: en_permutation_test(A_diff, size, delta), range(numSamples))))
		s_list.append(tok[1])
		l_list.append(tok[2])
		e_list.append(tok[3])
	estimates = np.array(estimates)
	s_mean = statistics.mean(s_list)
	l_mean = statistics.mean(l_list)
	e_mean = statistics.mean(e_list)
	s_std = statistics.pstdev(s_list)
	l_std = statistics.pstdev(l_list)
	e_std = statistics.pstdev(e_list)
	diffCount = 0
	estimates = sorted(estimates.tolist())
	for tok in estimates:
		if tok >= delta:
			diffCount += 1
	significance = 1 - float(diffCount) / float(numSamples)
	with open(sys.argv[1]+ '-dlm-pval.txt', 'w') as f:
		if significance <= 0.05 and significance > 0.025:
			f.write('p <= 0.05' + '\n')
		if significance <= 0.025 and significance > 0.005:
			f.write('p <= 0.025' + '\n')
		if significance <= 0.005 and significance > 0.001:
			f.write('p <= 0.005' + '\n')
		if significance <= 0.001:
			f.write('p <= 0.001' + '\n')
		f.write(str(round(s_mean * 100,2)) + '\n')
		f.write(str(round(s_std * 100,2)) + '\n')
		f.write(str(round(l_mean * 100,2)) + '\n')
		f.write(str(round(l_std * 100,2)) + '\n')
		f.write(str(round(e_mean * 100,2)) + '\n')
		f.write(str(round(e_std * 100,2)) + '\n')
		f.close()


if sys.argv[1] == 'ctb':
	results = list(map(lambda x: ja_permutation_test(A_diff, size, delta), range(numSamples)))
	for tok in results:
		estimates.append(tok[0])
#	estimates = np.array(list(map(lambda x: en_permutation_test(A_diff, size, delta), range(numSamples))))
		s_list.append(tok[1])
		l_list.append(tok[2])
		e_list.append(tok[3])
	estimates = np.array(estimates)
	s_mean = statistics.mean(s_list)
	l_mean = statistics.mean(l_list)
	e_mean = statistics.mean(e_list)
	s_std = statistics.pstdev(s_list)
	l_std = statistics.pstdev(l_list)
	e_std = statistics.pstdev(e_list)
	diffCount = 0
	estimates = sorted(estimates.tolist())
	for tok in estimates:
		if tok >= delta:
			diffCount += 1
	significance = 1 - float(diffCount) / float(numSamples)
	with open(sys.argv[1] + '-dlm-pval.txt', 'w') as f:
		if significance <= 0.05 and significance > 0.025:
			f.write('p <= 0.05' + '\n')
		if significance <= 0.025 and significance > 0.005:
			f.write('p <= 0.025' + '\n')
		if significance <= 0.005 and significance > 0.001:
			f.write('p <= 0.005' + '\n')
		if significance <= 0.001:
			f.write('p <= 0.001' + '\n')
		f.write(str(round(s_mean * 100,2)) + '\n')
		f.write(str(round(s_std * 100,2)) + '\n')
		f.write(str(round(l_mean * 100,2)) + '\n')
		f.write(str(round(l_std * 100,2)) + '\n')
		f.write(str(round(e_mean * 100,2)) + '\n')
		f.write(str(round(e_std * 100,2)) + '\n')
		f.close()


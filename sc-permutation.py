#usr/bin/env python3
import sys, re, glob, os, csv, io
import numpy as np
from collections import Counter
from numpy import random
import statistics

path = '/Users/Silverlining/Desktop/Experiments/Penn_PP/'
os.chdir(path)

data = []
a=0
b=0
with open(sys.argv[1] + '-vp-data.csv', newline = '') as f:
	reader = csv.reader(f)
	next(reader, None)

	for row in reader:
		if int(row[5]) - int(row[4]) < 0 and row[-2] != row[-1]:
			if row[-1] == 'Argument' and row[-2] == 'Adjunct':
				data.append(-1)
				a+=1
			if row[-2] == 'Argument' and row[-1] == 'Adjunct':
				data.append(1)
				b+=1
print(a)
print(b)

A_diff = np.array(data)

def en_permutation_test(pooled, size, delta):
	np.random.shuffle(pooled)
	s = 0
	l = 0
	A_sample = pooled[ : size]
	for i in range(0, size):
		if A_sample[i] >  0:
			s += 1
		if A_sample[i] <  0:
			l += 1
	return [s / size - l / size, s / size, l / size]

def ja_permutation_test(pooled, size, delta):
	np.random.shuffle(pooled)
	s = 0
	l = 0
	A_sample = pooled[ : size]
	for i in range(0, size):
		if A_sample[i] < 0:
			s += 1
		if A_sample[i] >  0:
			l += 1
	return [s / size - l / size, s / size, l / size]

pooled = np.hstack([A_diff])
delta = 0
size = 40
numSamples = 1000000
s_list = []
l_list = []
estimates = []
if sys.argv[1] in ['wsj', 'brown', 'swbd']:
	results = list(map(lambda x: en_permutation_test(A_diff, size, delta), range(numSamples)))
	for tok in results:
		estimates.append(tok[0])
#	estimates = np.array(list(map(lambda x: en_permutation_test(A_diff, size, delta), range(numSamples))))
		s_list.append(tok[1])
		l_list.append(tok[2])
	estimates = np.array(estimates)
	s_mean = statistics.mean(s_list)
	l_mean = statistics.mean(l_list)
	s_std = statistics.pstdev(s_list)
	l_std = statistics.pstdev(l_list)
	diffCount = 0
	estimates = sorted(estimates.tolist())
	for tok in estimates:
		if tok >= delta:
			diffCount += 1
	significance = 1 - float(diffCount) / float(numSamples)
	with open(sys.argv[1]+ '-long-sc-pval.txt', 'w') as f:
		if significance <= 0.05 and significance > 0.025:
			f.write('p <= 0.05' + '\n')
		if significance <= 0.025 and significance > 0.005:
			f.write('p <= 0.025' + '\n')
		if significance <= 0.005 and significance > 0.001:
			f.write('p <= 0.005' + '\n')
		if significance <= 0.001:
			f.write('p <= 0.001' + '\n')
		f.write(str(round(s_mean * 100,2)) + '\n')
		f.write(str(round(s_std * 100,2)) + '\n')
		f.write(str(round(l_mean * 100,2)) + '\n')
		f.write(str(round(l_std * 100,2)) + '\n')
		f.close()


if sys.argv[1] == 'ctb':
	results = list(map(lambda x: ja_permutation_test(A_diff, size, delta), range(numSamples)))
	for tok in results:
		estimates.append(tok[0])
#	estimates = np.array(list(map(lambda x: en_permutation_test(A_diff, size, delta), range(numSamples))))
		s_list.append(tok[1])
		l_list.append(tok[2])
	estimates = np.array(estimates)
	s_mean = statistics.mean(s_list)
	l_mean = statistics.mean(l_list)
	s_std = statistics.pstdev(s_list)
	l_std = statistics.pstdev(l_list)
	diffCount = 0
	estimates = sorted(estimates.tolist())
	for tok in estimates:
		if tok >= delta:
			diffCount += 1
	significance = 1 - float(diffCount) / float(numSamples)
	with open(sys.argv[1] + '-short-sc-pval.txt', 'w') as f:
		if significance <= 0.05 and significance > 0.025:
			f.write('p <= 0.05' + '\n')
		if significance <= 0.025 and significance > 0.005:
			f.write('p <= 0.025' + '\n')
		if significance <= 0.005 and significance > 0.001:
			f.write('p <= 0.005' + '\n')
		if significance <= 0.001:
			f.write('p <= 0.001' + '\n')
		f.write(str(round(s_mean * 100,2)) + '\n')
		f.write(str(round(s_std * 100,2)) + '\n')
		f.write(str(round(l_mean * 100,2)) + '\n')
		f.write(str(round(l_std * 100,2)) + '\n')
		f.close()


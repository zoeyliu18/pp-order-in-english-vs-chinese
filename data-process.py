import sys, csv
from collections import defaultdict

columns = defaultdict(list)

with open(sys.argv[1] + '-vp-data.csv', newline = '') as f:
	reader = csv.DictReader(f)
	for row in reader:
		for (k, v) in row.items():
			columns[k].append(v)

pp1_len = columns['PP1-len']
pp2_len = columns['PP2-len']
length = []
lendiff = []


if sys.argv[1] in ['wsj', 'brown', 'swbd']:
	for i in range(len(pp1_len)):
		if int(pp2_len[i]) > int(pp1_len[i]):
			length.append(1)
		if int(pp2_len[i]) < int(pp1_len[i]):
			length.append(-1)
		if int(pp2_len[i]) == int(pp1_len[i]):
			length.append(0)
		lendiff.append(int(pp2_len[i]) - int(pp1_len[i]))

if sys.argv[1] == 'ctb':
	for i in range(len(pp1_len)):
		if int(pp2_len[i]) < int(pp1_len[i]):
			length.append(1)
		if int(pp2_len[i]) > int(pp1_len[i]):
			length.append(-1)
		if int(pp2_len[i]) == int(pp1_len[i]):
			length.append(0)
		lendiff.append(int(pp1_len[i]) - int(pp2_len[i]))

freq = []
freqdiff = []

first_perplexity = []
with open(sys.argv[1] + '-first-perplexity.txt') as f:
	for line in f:
		first_perplexity.append(float(line))

second_perplexity = []
with open(sys.argv[1] + '-second-perplexity.txt') as f:
	for line in f:
		second_perplexity.append(float(line))

for i in range(len(first_perplexity)):
	if second_perplexity[i] > first_perplexity[i]:
		freq.append(1)
	if second_perplexity[i] < first_perplexity[i]:
		freq.append(-1)
	if second_perplexity[i] == first_perplexity[i]:
		freq.append(0)
	freqdiff.append(second_perplexity[i] - first_perplexity[i])

pp1 = []
pp2 = []

a = 0
b = 0
c = 0
d = 0
e = 0
h = 0
g = 0

with open(sys.argv[1] + '-pp1.txt') as f:
	for line in f:
		pp1.append(line.split())

with open(sys.argv[1] + '-pp2.txt') as f:
	for line in f:
		pp2.append(line.split())

for i in range(len(pp1)):
	if pp1[i][0] == 'from' and pp2[i][0] == 'to' and '%' in pp1[i] and '%' in pp2[i]:
		if second_perplexity[i] > first_perplexity[i]:
			a += 1
		if second_perplexity[i] < first_perplexity[i]:
			b += 1
		if second_perplexity[i] == first_perplexity[i]:
			c += 1
	if pp2[i][0] == 'from' and pp1[i][0] == 'to' and '%' in pp1[i] and '%' in pp2[i]:
		if second_perplexity[i] > first_perplexity[i]:
			e += 1
		if second_perplexity[i] < first_perplexity[i]:
			h += 1
		if second_perplexity[i] == first_perplexity[i]:
			g += 1

print(a)
print(b)
print(c)
print(e)
print(h)
print(g)

'''
sc = []
pp1_sc = columns['PP1-semantics']
pp2_sc = columns['PP2-semantics']

if sys.argv[1] in ['wsj', 'brown', 'swbd']:
	for i in range(len(pp1_sc)):
		if pp1_sc[i] == 'Argument' and pp2_sc[i] == 'Adjunct':
			sc.append(1)
		if pp1_sc[i] == 'Adjunct' and pp2_sc[i] == 'Argument':
			sc.append(-1)
		if pp1_sc[i] == ' ' and pp2_sc[i] == ' ':
			sc.append(0)

if sys.argv[1] == 'ctb':
	for i in range(len(pp1_sc)):
		if pp2_sc[i] == 'Argument' and pp1_sc[i] == 'Adjunct':
			sc.append(1)
		if pp2_sc[i] == 'Adjunct' and pp1_sc[i] == 'Argument':
			sc.append(-1)
		if pp1_sc[i] == ' ' and pp2_sc[i] == ' ':
			sc.append(0)

original = columns['Sentence']
variant = columns['Reverse_sentence']

reverse_len = []
reverse_lendiff = []
reverse_freq = []
reverse_freqdiff = []
reverse_sc = []

for tok in length:
	reverse_len.append(-1 * tok)

for tok in lendiff:
	reverse_lendiff.append(-1 * tok)

for tok in freq:
	reverse_freq.append(-1 * tok)

for tok in freqdiff:
	reverse_freqdiff.append(-1 * tok)

for tok in sc:
	reverse_sc.append(-1 * tok)

print(len(length))
print(len(lendiff))
print(len(freq))
print(len(first_perplexity))
print(len(second_perplexity))
print(len(freqdiff))
print(len(sc))
print(len(reverse_len))
print(len(reverse_lendiff))
print(len(reverse_freq))
print(len(reverse_freqdiff))
print(len(reverse_sc))

header = ['Sentence', 'Len', 'LenDiff', 'Freq', 'FreqDiff', 'SC', 'Order']

with open(sys.argv[1] + '-final.csv', 'w', encoding = 'utf-8') as data:
	writer = csv.writer(data)
	writer.writerow(header)
	for i in range(len(original)):
		writer.writerow([original[i], length[i], lendiff[i], freq[i], freqdiff[i], sc[i], 1])
	for i in range(len(original)):
		writer.writerow([variant[i], reverse_len[i], reverse_lendiff[i], reverse_freq[i], reverse_freqdiff[i], reverse_sc[i], 0])

'''





